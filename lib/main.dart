import 'dart:io';

import 'package:flutter/material.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:image_picker/image_picker.dart';

void main() {
  runApp(BarcodeApp());
}

class BarcodeApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Barcode App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BarcodeAppPage(),
    );
  }
}

class BarcodeAppPage extends StatefulWidget {
  BarcodeAppPage({Key key}) : super(key: key);

  @override
  _BarcodeAppPageState createState() => _BarcodeAppPageState();
}

class _BarcodeAppPageState extends State<BarcodeAppPage> {
  String _text = 'No result';
  final _textStyle = TextStyle(fontSize: 18.0);

  void _onPickPhotoClicked() async {
    File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    final FirebaseVisionImage visionImage =
        FirebaseVisionImage.fromFile(imageFile);
    final BarcodeDetector barcodeDetector =
        FirebaseVision.instance.barcodeDetector();
    List barcodeList = await barcodeDetector.detectInImage(visionImage);
    for (Barcode barcode in barcodeList) {
      _text = barcode.displayValue;
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Barcode App"),
        ),
        body: new Builder(builder: (BuildContext context) {
          return new Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new RaisedButton(
                    onPressed: _onPickPhotoClicked,
                    child: Text("Pick barcode")),
                Text(
                  _text,
                  style: _textStyle,
                )
              ],
            ),
          );
        }));
  }
}
